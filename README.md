A movement state machine that aims to mimic the movement in the Overwatch game.

**Features**
- Ground check using raycast
- Air influence when jumping
- First person camera
- Ability system

**W.I.P heroes**
- Widowmaker

**Working heroes**
- generic movement

-
Learn more, and catch us live here :
https://www.youtube.com/channel/UCtQPCnbIB7SP_gM1Xtv8bDQ