﻿using UnityEngine;
using System.Collections;

public class BaseState : MonoBehaviour
{
    public bool unlocked = true;

    protected BaseMotor motor;
    protected BaseHero hero;
    protected float startTime;
	protected float immuneTime;

	private void Awake ()
	{
		motor = GetComponent<BaseMotor> ();
        hero = GetComponent<BaseHero>();
    }

	public virtual void Construct()
	{
		startTime = Time.time;
	}
	public virtual void Destruct()
	{
		
	}
	public virtual void PlayerTransition()
	{
		if (Time.time - startTime < immuneTime)
			return;
	}
    public virtual void Transition()
    {
        if (Time.time - startTime < immuneTime)
            return;
    }
	public virtual void ProcessAnimation(Animator animator)
	{
		Vector3 dir = motor.MoveVector;
		MotorHelper.KillVertical (ref dir);
	}
	public virtual Vector3 ProcessMotion(Vector3 input)
	{
		Debug.Log ("Process Motion not implemented in " + this.ToString ());
		return input;
	}
	public virtual Quaternion ProcessRotation(Vector3 input)
	{
		return MotorHelper.FaceDirection (motor.MoveVector);
	}
    public virtual void AnimationEnded()
    {

    }
}
