﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

/* Debug class using reflection to access private fields */

public class HeroInfoText : MonoBehaviour
{
    public BaseMotor motor;
    public BaseHero hero;
    public Text stateText, ability1, ability2, ability3, ability4, ability5;

    private BaseAbility[] abs;

    private void Start()
    {
        abs = motor.GetComponents<BaseAbility>();
    }

    private void Update()
    {
        var state = typeof(BaseMotor).GetField("state", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(motor);
        stateText.text = state.ToString();
        ability1.text = "LEFT: " + abs[0].GetCooldown().ToString("0.00");
        ability2.text = "RIGHT: " + abs[1].GetCooldown().ToString("0.00");
        ability3.text = "SHIFT: " + abs[2].GetCooldown().ToString("0.00");
        ability4.text = "E: " + abs[3].GetCooldown().ToString("0.00");
        ability5.text = "Q: " + abs[4].GetCooldown().ToString("0.00");
    }
}
