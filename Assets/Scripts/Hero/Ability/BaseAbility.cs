﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAbility : MonoBehaviour
{
    public float cooldown = 0.5f;
    protected float lastCast;

    protected BaseHero thisHero;

    protected virtual void Start()
    {
        thisHero = GetComponent<BaseHero>();
    }
    public virtual void Cast()
    {
        if (IsReady())
        {
            lastCast = Time.time;
        }
    }
    protected bool IsReady()
    {
        return Time.time - lastCast > cooldown;
    }

    public float GetCooldown()
    {
        float c = (cooldown - (Time.time - lastCast));
        return (c <= 0) ? 0 : c;
    }
    public BaseHero GetHero()
    {
        if(!thisHero)
            thisHero = GetComponent<BaseHero>();

        return thisHero;
    }
}
